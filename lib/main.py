#
# This file is part of auto-ipsec.
#
# auto-ipsec is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# auto-ipsec is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with auto-ipsec. If not, see <http://www.gnu.org/licenses/>.
#

import logging
import logging.handlers


def setup_logging(config):
    if not config.log.enabled:
        # disable all the logging across application
        logging.disable(logging.CRITICAL)
        return

    root_logger = logging.getLogger("")
    root_logger.setLevel(config.log.level)

    syslog = logging.handlers.SysLogHandler(address="/dev/log", facility=logging.handlers.SysLogHandler.LOG_DAEMON)
    root_logger.addHandler(syslog)

    if config.log.file is not None:
        log_file = logging.handlers.RotatingFileHandler(filename=config.log.file, maxBytes=10485760, backupCount=25)
        root_logger.addHandler(log_file)


if __name__ == "__main__":
    pass
