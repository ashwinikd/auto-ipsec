#
# This file is part of auto-ipsec.
#
# auto-ipsec is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# auto-ipsec is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with auto-ipsec. If not, see <http://www.gnu.org/licenses/>.
#

import ConfigParser
import io
import logging


class NetworkConfig:
    """
    Network configuration
    """

    def __init__(self):
        """ IP Address to listen on for IPSec updates from other auto-ipsec enabled hosts """
        self.listen = ""

        """ Port to listen on for IPSec updates from other auto-ipsec enabled hosts """
        self.port = 87107

        """ Multicast group address """
        self.group_address = "239.255.107.87"

        """ Multicast group port """
        self.group_port = 87107

        """ 
        Multicasting TTL. By default within same subnet. See http://www.tldp.org/HOWTO/Multicast-HOWTO-2.html 
        """
        self.ttl = 1

    def read(self, config):
        if config.has_option("network", "listen"):
            self.listen = config.get("network", "listen")

        if config.has_option("network", "port"):
            self.port = config.getint("network", "port")

        if config.has_option("network", "group_address"):
            self.group_address = config.get("network", "group_address")

        if config.has_option("network", "group_port"):
            self.group_port = config.getint("network", "group_port")

        if config.has_option("network", "ttl"):
            self.ttl = config.getint("network", "ttl")


class LoggingConfig:
    def __init__(self):
        self.enabled = True
        self.file = None
        self.level = logging.INFO

    def read(self, config):
        if config.has_option("log", "enable"):
            self.enabled = config.getboolean("log", "enable")

        if config.has_option("log", "file"):
            self.file = config.getboolean("log", "file")

        if config.has_option("log", "level"):
            self.enabled = config.getint("log", "level") * 10


class Config:
    """
    Main Application configuration
    """

    def __init__(self, file_path):
        self._config_file_path = file_path
        self.network = NetworkConfig()
        self.log = LoggingConfig()

    def read(self):
        with open(self._config_file_path, 'r') as f:
            main_config = f.read()

        config = ConfigParser.RawConfigParser(allow_no_value=True)
        config.readfp(io.BytesIO(main_config))

        if config.has_section("network"):
            self.network.read(config)

        if config.has_section("log"):
            self.log.read(config)
