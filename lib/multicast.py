#
# This file is part of auto-ipsec.
#
# auto-ipsec is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# auto-ipsec is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with auto-ipsec. If not, see <http://www.gnu.org/licenses/>.
#

import socket
import struct
import threading


class MulticastListener:
    """
    Abstract class for listening to multicast messages.
    """

    def __init__(self):
        pass

    def on_host_up(self, host):
        """
        Called when alive notification from a host is received.
        :param host: Host details
        :return: void
        """
        raise NotImplementedError("Should have implemented this")


class MulticastServerReadingThread(threading.Thread):
    def __init__(self, sock, handler):
        super(MulticastServerReadingThread, self).__init__()
        self._socket = sock
        self._handler = handler
        self._stop = False

    def __stop(self):
        self._stop = True

    def run(self):
        while not self._stop:
            data, sender = self._socket.recvfrom(1500)
            print (str(sender) + '  ' + repr(data))
            # TODO: call handler method


class MulticastServer:
    """ Multicast listening server """

    def __init__(self, config, handler):
        """
        :param config: Config application configuration
        :param handler: MulticastListener handler for host up events 
        """
        self._group = config.network.group_address
        self._listen = config.network.listen
        self._addrinfo = socket.getaddrinfo(self._group, None)[0]
        self._port = config.network.group_port
        self._ttl = config.network.ttl
        self.running = False
        self._handler = handler
        self._socket = None
        self._reading_thread = None

    def start(self):
        """
        Start this multicast server
        :return: void 
        """
        # First let others know that we are alive
        self._multicast_alive_event()

        # Start listening for multicast events
        self._socket = socket.socket(self._addrinfo[0], socket.SOCK_DGRAM)

        # Make the socket multicast friendly by allowing other applications on the
        # same machine to join the multicast group.
        self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        if hasattr(self._socket, "SO_REUSEPORT"):
            self._socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)

        # Bind the socket
        self._socket.bind((self._listen, self._port))

        # Join group
        group_bin = socket.inet_pton(self._addrinfo[0], self._addrinfo[4][0])
        if self._addrinfo[0] == socket.AF_INET:  # IPv4
            mreq = group_bin + struct.pack('=I', socket.inet_aton(self._listen))
            self._socket.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        else:
            mreq = group_bin + struct.pack('@I', 0)
            self._socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_JOIN_GROUP, mreq)

        # Start reading from socket
        self._reading_thread = MulticastServerReadingThread(self._socket, self._handler)
        self._reading_thread.start()

        self.running = True

    def stop(self):
        """
        Stop the server
        :return: void
        """
        self.running = False

        if self._reading_thread is not None and self._reading_thread.is_alive():
            self._reading_thread.stop()
            self._reading_thread.join()
            self._reading_thread = None

        if self._socket is not None:
            group_bin = socket.inet_pton(self._addrinfo[0], self._addrinfo[4][0])
            if self._addrinfo[0] == socket.AF_INET:  # IPv4
                mreq = group_bin + struct.pack('=I', socket.inet_aton('0.0.0.0'))
                self._socket.setsockopt(socket.IPPROTO_IP, socket.IP_DROP_MEMBERSHIP, mreq)
            else:
                mreq = group_bin + struct.pack('@I', 0)
                self._socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_LEAVE_GROUP, mreq)
            self._socket.close()
            self._socket = None

    def _multicast_alive_event(self):
        s = socket.socket(self._addrinfo[0], socket.SOCK_DGRAM)
        ttl_bin = struct.pack('@i', 1)

        if self._addrinfo[0] == socket.AF_INET:  # IPv4
            s.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, ttl_bin)
        else:
            s.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_MULTICAST_HOPS, ttl_bin)

        s.sendto('\0', (self._addrinfo[4][0], self._port))
        s.close()
